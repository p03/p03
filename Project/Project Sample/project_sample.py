#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import hashlib as hasher
import datetime as date
import sys
import math
import numpy as np
import unittest


# A function used to indicate whether a string could be converted to double
def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


def nb_classifier(training_file):
    # Read the training file and process the data into 2D list
    training_file_object = open(training_file, "r")
    training_lines = training_file_object.readlines();
    # Initialise the training list
    training_list = []
    for i in range((len(training_lines[0].rstrip().split(',')) - 1) * 2):
        training_list.append([])
    # Create a variable to count the number of no
    number_no = 0
    # Process the training_lines data and put it into a list that contains 2 * NUM_ATTRIBUTE list
    # The first NUM_ATTRIBUTE lists contain the list of 1st, 2nd, ... NUM_ATTRIBUTEth attrbitute of data instances which are classified as yes
    # The second NUM_ATTRIBUTE lists contain the list of 1st, 2nd, ... NUM_ATTRIBUTEth attrbitute of data instances which are classified as no
    for line in training_lines:
        # Transform the string into a list by delimiting the commas
        data_instance = line.rstrip().split(',')
        # Set up a variable to help to decide which list to append when inserting the attribute of each data instance
        temp = 0
        if (data_instance[-1] == 'no'):
            temp = 1
            number_no += 1
        # Insert the attributes into each list of the data_instance
        for i in range(len(data_instance) - 1):
            if (isfloat(data_instance[i])):
                training_list[temp * (len(data_instance) - 1) + i].append(float(data_instance[i]))
            else:
                print("Invalid attribute value")
                sys.exit()
    # Read the testing file
    testing_file_object = open("pima_test.csv", "r")
    answer = open("pimaa.csv","r")
    testing_lines = testing_file_object.readlines()
    answers = answer.readline().rstrip().split(',')
    answer_irterator = 0
    correct_count = 0
    for line in testing_lines:
        test = line.rstrip().split(',')
        probability_yes = (len(training_lines) - number_no) / len(training_lines)
        probability_no = number_no / len(training_lines)
        # Calculate the probability of class yes and class no
        for i in range(len(test)):
            # Calculate the probability of the current attribute under the class yes
            if isfloat(test[i]):
                current_attribute_prob_yes = math.exp(-math.pow(float(test[i]) - np.mean(training_list[i]), 2) / (
                        2 * math.pow(np.std(training_list[i], ddof=1), 2))) / (
                                                     np.std(training_list[i]) * math.sqrt(2 * math.pi))
                probability_yes *= current_attribute_prob_yes
                current_attribute_prob_no = math.exp(
                    -math.pow(float(test[i]) - np.mean(training_list[i + len(test)]), 2) / (
                            2 * math.pow(np.std(training_list[i + len(test)], ddof=1), 2))) / (
                                                    np.std(training_list[i + len(test)]) * math.sqrt(2 * math.pi))
                probability_no *= current_attribute_prob_no
            else:
                print("Invalid attribute value")
                sys.exit()
        if (probability_yes >= probability_no and answers[answer_irterator] == "yes")or (probability_yes < probability_no and answers[answer_irterator] == "no"):
            print("correct")
            correct_count+=1
        else:
            print("wrong")
        answer_irterator+=1
    print("Correctness", str(correct_count/answers.__len__()*100)+"% out of", str(answers.__len__()), "tests")


class Block:
    def __init__(self, index, timestamp, data, previous_hash):
        self.index = index
        self.timestamp = timestamp
        self.data = data
        self.previous_hash = previous_hash
        self.hash = self.hash_block()

    def hash_block(self):
        sha = hasher.sha256()
        sha.update(
            bytes(
                str(self.index) + str(self.timestamp) + str(self.data) + str(
                    self.previous_hash), 'utf-8'))
        return sha.hexdigest()


def create_genesis_block():
    #  Manually construct a block with index 0 and arbitrary previous hash
    return Block(0, date.datetime.now(), nb_classifier, "0")


def next_block(last_block,data):
    this_index = last_block.index + 1
    this_timestamp = date.datetime.now()
    this_data = data
    this_hash = last_block.hash
    return Block(this_index, this_timestamp, this_data, this_hash)


def setup_blockchain():
    blockchain = [create_genesis_block()]
    previous_block = blockchain[0]
    return blockchain, previous_block


def add_blocks(num_of_blocks_to_add):
    blockchain, previous_block = setup_blockchain()
    for i in range(0, int(num_of_blocks_to_add)):
        block_to_add = next_block(previous_block,i)
        blockchain.append(block_to_add)
        previous_block = block_to_add
        print("Block #{} has been added to the "
              "blockchain!".format(block_to_add.index))
        if format(block_to_add.index) == "1":
            print("This is the genesis block that recorded the contract")
        print("Hash: {}\n".format(block_to_add.hash))
    return blockchain


def add_a_block(file):
    blockchain, previous_block = setup_blockchain()
    block_to_add = next_block(previous_block, file)
    blockchain.append(block_to_add)
    return block_to_add

def test_data(file):
    blockchain, previous_block = setup_blockchain()
    block_to_add = next_block(previous_block, file)
    blockchain.append(block_to_add)
    blockchain[0].data(blockchain[1].data)

def main():
    #  Create the blockchain and add the genesis block
    blockchain, previous_block = setup_blockchain()

    #  How many blocks should we add to the chain after the genesis block
    num_of_blocks_to_add = 20

#test1

        #  Tell everyone about it!


#test2
    block_to_add = next_block(previous_block,"pima.csv")
    blockchain.append(block_to_add)
    print("Block #{} has been added to the "
               "blockchain!".format(block_to_add.index))
    print("This is the genesis block that recorded the contract.")
    print("Hash: {}\n".format(block_to_add.hash))
    print("")
    print("now we are going to test the Data on the block")
    print(blockchain[0].data(blockchain[1].data))


if __name__ == "__main__":
    main()
