#some basic imports and setups
import os
import cv2
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import sys
from alexnet import AlexNet
from caffe_classes import class_names

def save_to_file(text,path):
    with open(path, mode='wt', encoding='utf-8') as myfile:
        myfile.write('\n'.join(text))
        myfile.write('\n')

def classify(image_dir=None,result_path=None):
    #mean of imagenet dataset in BGR
    imagenet_mean = np.array([104., 117., 124.], dtype=np.float32)

    file_path = os.path.dirname(os.path.realpath(__file__))
    #get list of all images
    if(image_dir==None):
        image_dir = file_path+"/../../media/data"
    if(result_path==None):
        result_path = file_path

    img_files = [os.path.join(image_dir, f) for f in os.listdir(image_dir)  if f.endswith('.jpeg') or f.endswith('.jpg') or f.endswith('.png')]
    num_files = len(img_files)
    #load all images
    imgs = []
    for f in img_files:
        imgs.append(cv2.imread(f))

    x = tf.placeholder(tf.float32, [1, 227, 227, 3])
    keep_prob = tf.placeholder(tf.float32)
    model = AlexNet(x, keep_prob, 1000, [],weights_path=file_path+'/bvlc_alexnet.npy')
    score = model.fc8
    softmax = tf.nn.softmax(score)
    dog_from = 151
    dog_to = 275
    predict_output = []
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        model.load_initial_weights(sess)
        fig = plt.figure(figsize=(15,20))
        for i, image in enumerate(imgs):
            # Convert image to float32 and resize to (227x227)
            img = cv2.resize(image.astype(np.float32), (227,227))
            # Subtract the ImageNet mean
            img -= imagenet_mean
            # Reshape as needed to feed into model
            img = img.reshape((1,227,227,3))
            # Run the session and calculate the class probability
            probs = sess.run(softmax, feed_dict={x: img, keep_prob: 1})
            # Get the class name of the class with the highest probability
            class_num = np.argmax(probs)
            isDog = dog_from <= class_num <= dog_to
            # Plot image with class name and prob in the title
            fig.add_subplot(num_files/5+1,5,i+1)
            plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
            if(isDog):
                output_text = "It is a dog"
                predict_output.append(img_files[i]+' '+str(1))
            else:
                output_text = "It's not a dog"
                predict_output.append(img_files[i]+' '+str(0))
            plt.title(output_text)
            plt.axis('off')

    fig.savefig(result_path+'/results.png')
    save_to_file(predict_output,result_path+'/results.txt')
    plt.close()
