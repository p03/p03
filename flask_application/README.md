# Blockchain Boosting Trustworthy AI

Flask Application that uses Ethereum Blockchain to submit Data against a ML model and recieve feedback on various aspects of the Data.

## Install Dependencies


### Installing the Solidity Compiler

**For OSX**
Install a these homebrew packages:
```
brew install pkg-config libffi autoconf automake libtool openssl
```
Install the solidity compiler (solc):
```
brew update
brew upgrade
brew tap ethereum/ethereum
brew install solidity
brew link solidity
```
**For Linux**
```
sudo add-apt-repository ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get install solc libssl-dev
```

### Firstly you need to install Ganache CLI
  If not already installed, install NodeJS: https://nodejs.org/en/
  This is a local  blockchain for Ethereum development. It simulates client behavior and make developing Ethereum applications faster, easier, and safer.  

  To install Ganache CLI type in the following command in terminal.
  ```
  sudo npm install -g ganache-cli
  ```
### Initialize your Virtual Environment

Install [virtualenv](https://virtualenv.pypa.io/en/stable/) if you don't have it yet. (Comes installed with [Python3.6](https://www.python.org/downloads/))

Setup a virtual environment with Python 3:
```
cd flask_application
python3.6 -m venv venv
source venv/bin/activate
```
### Install python dependencies
  Type the following command in the terminal to install the python libraries:
  ```
  pip3 install -r requirements.txt
  ```

## Run Application

### Run the Ganache CLI
You will need to run the ganache-cli to run a blockchain locally for the application to connect to.

Open a new tab and type in the following command in terminal:
 ```
 ganache-cli
 ```
### Deploy Contract and Run Flask
Back to the inital terminal window where your venv is activated, make sure you are in the directory containg the main.py file.

Run the bash script to deploy the smart contract and run Flask Environment:
 ```
 bash ./run.sh
 ```
 Open the link running the application on http://127.0.0.1:5000/

## Run Tests
Ensure you have pytests installed on your computer:
```
pip3 install pytest
```

Change into the directory containing all the tests in terminal. Type in the following command:
```
pytest
```
This will run all the tests and give feedback on which ones where sucessfull and any which failed.
