pragma solidity ^0.4.24;

contract DataSubmission {
  //Structure for each submission
  struct Submission {
    uint8 evaluation;
    uint8 similarity;
    uint8 score;
    uint16 rank;
  }
  // Mapping for each address to be mapped to a submission
  mapping (address => Submission) public submissions;
  // To store the address of the best submission
  address[] indexes;
  address best_submission;
  // To store total number of submission
  uint16 total_submissions = 0;

  //Constructor
  constructor() public {
  }
  //Creates a Submission from the data and stores it in the Submissions mapping
  function submitData(address _account, uint8 _evaluation, uint8 _similarity, uint8 _score) public {
      total_submissions++;
      Submission memory s = Submission(_evaluation,_similarity,_score,total_submissions);
      submissions[_account] = s;
      indexes.push(_account);
  }
  // Setter to set Rank, Not yet complete
  function setRank(address _account) public{
      submissions[_account].rank = total_submissions;
  }
  // Getter to get similarity for an address
  function getSimilarity(address _account) view public returns (uint8){
      return submissions[_account].similarity;
  }
  // Getter to get evaluation for an address
  function getEvaluation(address _account) view public returns (uint8){
      return submissions[_account].evaluation;
  }
  // Getter to get score for an address
  function getScore(address _account) view public returns (uint8){
      return submissions[_account].score;
  }
  // Getter to get senders Address, this is differen to other address' as they are the ethereum account address
  function senderAddress() view public returns (address){
      return msg.sender;
  }

  function getSubmission(uint16 index) view public returns (uint8,uint8,uint8){
      uint8 similarity = submissions[indexes[index]].similarity;
      uint8 evaluation = submissions[indexes[index]].evaluation;
      uint8 score = submissions[indexes[index]].score;
      return (similarity,evaluation,score);
  }

  function getRank(address _account) view public returns (uint16){
      return submissions[_account].rank;
  }

  function getTotalSubmissions() view public returns (uint16){
      return total_submissions;
  }
  function getSubmissionAccount(uint256 index) view public returns (address){
      return indexes[index];
  }

}
