#pip3 install pytest
#ganache cli must be running aswell
# in terminal cd to tests and type pytest
import sys
sys.path.append('../')
from solc import link_code
import json
from compile_solidity_utils import w3
import compile_solidity_utils as utils
# Solidity source code


def deploy():
    #Set default account
    contract_address, abi = utils.deploy_n_transact(['../submission.sol', '../stringUtils.sol'])
    w3.eth.defaultAccount = w3.eth.accounts[1]
    # Create the contract instance with the newly-deployed address
    user = w3.eth.contract(
        address=contract_address, abi=abi,
    )
    data_quality = 60
    data_similarity = 70
    data_score = int((data_quality+data_similarity)/2)
    #Submit the Results
    tx_hash = user.functions.submitData(w3.eth.accounts[1],data_quality,data_similarity,data_score)
    tx_hash = tx_hash.transact()
    w3.eth.waitForTransactionReceipt(tx_hash)
    #Store results retrieved from Blockchain and send to data_evaluation page
    res = user.functions.getScore(w3.eth.accounts[1]).call()
    return res

def test_score():
    assert deploy() == 65
