marshmallow>=2.15.4
Flask>=1.0.2
web3>=4.5.0
WTForms>=2.2.1
Flask_WTF>=0.14.2
numpy>=1.13.3
py-solc>=3.1.0
scikit_learn>=0.20.0
