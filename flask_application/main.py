import json
import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix,accuracy_score, precision_score,recall_score,f1_score
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import KFold
from sklearn.externals import joblib
from compile_solidity_utils import w3
from flask import Flask, Response, request, jsonify, render_template, redirect,url_for, flash
from marshmallow import Schema, fields, ValidationError
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired

# Function from ML code from other group, not yet utilised
def Model():
    data = np.genfromtxt('data.csv',dtype = None, encoding = 'UTF-8', names = True, delimiter = ',')
    # preprocessing
    encoder = preprocessing.LabelEncoder()
    encoder.fit(data['diagnosis'])
    # encoder.transform(data['diagnosis'])
    diagnosis = encoder.transform(data['diagnosis']).astype(int)
    new_data = diagnosis.reshape(-1,1)
    for i in range(2,len(data.dtype.names)):
        new_data = np.concatenate((new_data,np.expand_dims(data[data.dtype.names[i]],axis = 0).T),axis = 1)
    new_data[:,1:] = preprocessing.normalize(new_data[:,1:],axis = 0)
    x_train,x_test,y_train, y_test = train_test_split(new_data[:,1:],new_data[:,0],test_size = .2, random_state = 42)
    np.save('admin_training',x_train)
    np.save('admin_y',y_train)
    np.save('client_training',x_test)
    np.save('client_y',y_test)
    # 10 fold validation
    kf = KFold(n_splits = 10)
    counter = 1
    for train_index, test_index in kf.split(x_train):
        training_samples = x_train[train_index]
        training_y = y_train[train_index]
        test_samples = x_train[test_index]
        test_y = y_train[test_index]
        lda = LinearDiscriminantAnalysis()
        lda.fit(training_samples,training_y)
        predict_y = lda.predict(test_samples)
    #     print("test_y: {}".format(test_y))
    #     print("predict_y: {}".format(predict_y))
        file = open('model_evaluation_{}.txt'.format(counter),'w+')
        file.write("iteration: {}\n".format(counter))
        file.write("confusion matrix: {}\n".format(confusion_matrix(test_y,predict_y)))
        file.write("precision: {}\n".format(precision_score(test_y,predict_y)))
        file.write("recall: {}\n".format(recall_score(test_y,predict_y)))
        file.write("f1-score: {}\n".format(f1_score(test_y,predict_y)))
        file.close()
        # to store the model
        joblib.dump(lda, 'model_{}'.format(counter))
        counter += 1


class Login(FlaskForm):
    username = StringField('Name', validators=[DataRequired()])
    password = StringField('Password', validators=[DataRequired()])
    submit = SubmitField('Submit')

class Register(FlaskForm):
    username = StringField('Name', validators=[DataRequired()])
    password = StringField('Password', validators=[DataRequired()])
    confirm_password = StringField('Confirm Password', validators=[DataRequired()])
    submit = SubmitField('Submit')

class Submission(FlaskForm):
    data = StringField('Data', validators=[DataRequired()])
    submit = SubmitField('Submit')

class Reward(FlaskForm):
    number = StringField("Number", validators=[DataRequired()])
    submit = SubmitField('Submit')

app = Flask(__name__)
app.config['SECRET_KEY'] = 'any secret string'

@app.route('/')
def begin():
    return redirect('login')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        if request.form['username'] != 'data61@gmail.com' or request.form['password'] != '123':
            return  redirect("index")
        else:
            return redirect('account_selection')
    return render_template("index.html")

@app.route('/index')
def index():
    return render_template("index.html")

@app.route("/account_selection", methods =['POST'])
def account_selection():
    return render_template("account_selection.html")


@app.route("/admin_home")
def admin_home():
    return render_template("admin_home.html")


@app.route("/admin_profile")
def admin_profile():
    with open("data.json", 'r') as f:
        datastore = json.load(f)
    abi = datastore["abi"]
    contract_address = datastore["contract_address"]
    # Create the contract instance with the newly-deployed address
    user = w3.eth.contract(
        address=contract_address, abi=abi,
    )
    a = w3.eth.accounts[1]
    b = w3.fromWei(w3.eth.getBalance(a), 'ether')
    return render_template("admin_profile.html", address = a, balance = b)

@app.route("/admin_submission",methods = ['POST', 'GET'])
def admin_submission():
    reward = Reward()
    with open("data.json", 'r') as f:
        datastore = json.load(f)
    abi = datastore["abi"]
    contract_address = datastore["contract_address"]
    # Create the contract instance with the newly-deployed address
    user = w3.eth.contract(
        address=contract_address, abi=abi,
    )
    results = []
    for i in range(int(user.functions.getTotalSubmissions().call())):
        results.append([str(user.functions.getSubmissionAccount(i).call()),user.functions.getSubmission(i).call()])
    if request.method == 'POST':
        sender = w3.eth.accounts[1]
        accounts = request.form['number'].split(",")
        for i in accounts:
            receiver = address = w3.toChecksumAddress(results[int(i)][0])
            amount = w3.toWei(0.1, "ether")
            w3.eth.sendTransaction({'from': sender, 'to': receiver, 'value': amount})
        b = w3.fromWei(w3.eth.getBalance(sender), 'ether')
        return render_template("admin_profile.html", address = sender, balance = b)
    return render_template("admin_submission.html",submission = results, form = reward)

@app.route("/data_evaluation")
def data_evaluation():
    with open("current_user.json", 'r') as u:
        userstore = json.load(u)
    name = userstore["username"]
    address = w3.toChecksumAddress(userstore["address"])
    with open("data.json", 'r') as f:
        datastore = json.load(f)
    abi = datastore["abi"]
    contract_address = datastore["contract_address"]
    user = w3.eth.contract(
        address=contract_address, abi=abi,
    )

    eval = user.functions.getEvaluation(address).call()
    sim = user.functions.getSimilarity(address).call()
    res = user.functions.getScore(address).call()
    sub_rank = user.functions.getRank(address).call()
    sub_total = user.functions.getTotalSubmissions().call()
    balances = []
    return render_template("data_evaluation.html",evaluation=eval, similarity=sim, score = res, rank = sub_rank, total = sub_total)
    #return render_template("data_evaluation.html")


@app.route("/data_submission")
def data_submission():
    return render_template("data_submission.html")


@app.route("/model_evaluation")
def model_evaluation():
    return render_template("model_evaluation.html")


@app.route("/model_search")
def model_search():
    return render_template("model_search.html")


@app.route("/register")
def register():
    register = Register()
    return render_template("register.html",title="Register", form=register)


@app.route("/user_home")
def user_home():
    submission = Submission()
    if submission.validate_on_submit():
        #flash('Data submitted for User:{}'.format(form.username.data))
        return redirect(url_for("home"))
    return render_template("user_home.html",title="Submission", form=submission)


@app.route("/user_profile")
def user_profile():
    with open("current_user.json", 'r') as u:
        userstore = json.load(u)
    u = userstore["username"]
    a = w3.toChecksumAddress(userstore["address"])
    with open("data.json", 'r') as f:
        datastore = json.load(f)
    abi = datastore["abi"]
    contract_address = datastore["contract_address"]
    # Create the contract instance with the newly-deployed address
    user = w3.eth.contract(
        address=contract_address, abi=abi,
    )
    b = w3.fromWei(w3.eth.getBalance(a), 'ether')
    return render_template("user_profile.html", name = u, username = u, address = a, balance = b)


@app.route("/account_selection")
def account():
    return render_template("account_selection.html")

@app.route("/home")
def home():
    return render_template("home.html")


@app.route("/make_account", methods=['GET','POST'])
def make_account():
    name = request.form['username']
    password = request.form['password']
    confirm_password = request.form['confirm_password']
    address = w3.personal.newAccount(password)
    with open('current_user.json', 'w') as outfile:
        user = {
            "username": name,
            "password": password,
            "address": address
        }
        json.dump(user, outfile, indent=4, sort_keys=True)
    return redirect("user_home")

@app.route("/blockchain/user", methods=['GET','POST'])
def transaction():
    # Get Data from Form
    #name = request.form['username']
    data = request.form['data']
    with open("current_user.json", 'r') as u:
        userstore = json.load(u)
    name = userstore["username"]
    address = w3.toChecksumAddress(userstore["address"])
    #Set default account
    w3.eth.defaultAccount = w3.eth.accounts[1]
    #Open json file to get contract ABI (Application Binary Interface) and address
    with open("data.json", 'r') as f:
        datastore = json.load(f)
    abi = datastore["abi"]
    contract_address = datastore["contract_address"]
    # Create the contract instance with the newly-deployed address
    user = w3.eth.contract(
        address=contract_address, abi=abi,
    )
    #Sending money between accounts
    sender = w3.eth.accounts[1]
    receiver = w3.eth.accounts[5]
    amount = w3.toWei(0.1, "ether")
    w3.eth.sendTransaction({'from': sender, 'to': receiver, 'value': amount})
    # Generate some results
    if(data == "test"):
        data_quality = 60
        data_similarity = 70
    else:
        data_quality = 80
        data_similarity = 70
    data_score = int((data_quality+data_similarity)/2)

    #Submit the Results
    tx_hash = user.functions.submitData(address,data_quality,data_similarity,data_score)
    tx_hash = tx_hash.transact()
    w3.eth.waitForTransactionReceipt(tx_hash)

    #Store results retrieved from Blockchain and send to data_evaluation page
    eval = user.functions.getEvaluation(address).call()
    sim = user.functions.getSimilarity(address).call()
    res = user.functions.getScore(address).call()
    sub_rank = user.functions.getRank(address).call()
    sub_total = user.functions.getTotalSubmissions().call()
    return render_template("data_evaluation.html",evaluation=eval, similarity=sim, score = res, rank = sub_rank, total = sub_total)
