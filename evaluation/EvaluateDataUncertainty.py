import random
import numpy as np
import pandas as pd
from pandas import DataFrame as df
from sklearn.metrics import confusion_matrix
import math
import matplotlib.pyplot as plt
import pylab


def getRandomTestSet(data):
    index = 5
    random.shuffle(data)
    division = len(data) / float(index)
    X_train = [data[int(round(division * i)): int(round(division * (i + 1)))] for i in range(index)]
    return X_train

def get_data_portion(data_url):

    gapminder = pd.read_csv(data_url)
    size = int(len(gapminder)*0.8)
    random_subset = gapminder.sample(n=size,replace=True)
    print(random_subset)

def my_formula(x):
    return x**3+2*x-4

def graph(formula, x_range):
    x = np.array(x_range)
    y = formula(x)

    plt.plot(x, y)
    plt.show()

def sigmoid(x):
  return 1 / (1 + np.exp(10*(-x+0.5)))

def main():
    # y_true = [2, 0, 2, 2, 0, 1]
    # y_pred = [0, 0, 2, 2, 0, 2]
    #
    # cf = confusion_matrix(y_true, y_pred)
    print(sigmoid(1))
    x = np.linspace(-15, 15, 100)  # 100 linearly spaced numbers
    y = np.sin(x) / x  # computing the values of sin(x)/x

    # compose plot
    pylab.plot(x, y)  # sin(x)/x
    pylab.plot(x, y, 'co')  # same function with cyan dots
    pylab.plot(x, 2 * y, x, 3 * y)  # 2*sin(x)/x and 3*sin(x)/x
    pylab.show()  # show the plot


main()