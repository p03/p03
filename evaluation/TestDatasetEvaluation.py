import unittest
import multiDatasetEvaluation
from EvaluatedDataset import EvaluatedDataset

class TestMultiDatasetEvaluation(unittest.TestCase):

    def test_GetConfusionMatrix(self):
        predicted = [0,0,0,0]
        actual = [0,0,0,0]
        (TP, TN, FP, FN) = multiDatasetEvaluation.GetConfusionMatrix(predicted, actual)
        self.assertEqual((TP, TN, FP, FN),(0,4,0,0))

        predicted = [1,1,0,0]
        actual = [0,0,0,0]
        (TP, TN, FP, FN) = multiDatasetEvaluation.GetConfusionMatrix(predicted, actual)
        self.assertEqual((TP, TN, FP, FN),(0,2,2,0))

        predicted = [0,0,1,1,1,1,0,0]
        actual = [1,1,0,0,1,1,0,0]
        (TP, TN, FP, FN) = multiDatasetEvaluation.GetConfusionMatrix(predicted, actual)
        self.assertEqual((TP, TN, FP, FN),(2,2,2,2))

        predicted = [1]
        actual = [1]
        (TP, TN, FP, FN) = multiDatasetEvaluation.GetConfusionMatrix(predicted, actual)
        self.assertEqual((TP, TN, FP, FN),(1,0,0,0))
        return

        predicted = []
        actual = []
        (TP, TN, FP, FN) = multiDatasetEvaluation.GetConfusionMatrix(predicted, actual)
        self.assertEqual((TP, TN, FP, FN),(0,0,0,0))
        return

    def test_FillDatasetConfusionMatrix(self):
        dataset = EvaluatedDataset
        dataset.predicted = [0,0,0,0]
        dataset.actual = [0,1,1,1]
        multiDatasetEvaluation.FillDatasetObject(dataset)
        self.assertEqual(dataset.TP, 0)
        self.assertEqual(dataset.FP, 0)
        self.assertEqual(dataset.TN, 1)
        self.assertEqual(dataset.FN, 3)
        return

class TestEvaluatedDataset(unittest.TestCase):

    def test_getLength(self):
        dataset = EvaluatedDataset
        dataset.predicted = [1,0,0,1,1,1,0,1,0,0,0,1]
        dataset.actual = [0,0,1,1,1,0,1,1,0,1,1,1]
        multiDatasetEvaluation.FillDatasetObject(dataset)
        self.assertEqual(dataset.getLength(dataset), 12)
        return

    def test_getAccuracy(self):
        dataset = EvaluatedDataset
        dataset.predicted = [1,0,0,1,1,1,0,1,0,0,0,1]
        dataset.actual =    [0,0,1,1,1,0,1,1,0,1,1,1]
        multiDatasetEvaluation.FillDatasetObject(dataset)
        self.assertEqual(dataset.getAccuracy(dataset), 0.5)
        return

    def test_getPrecision(self):
        dataset = EvaluatedDataset
        dataset.predicted = [1,0,0,1,1,1,0,1,0,0,0,1]
        dataset.actual =    [0,0,1,1,1,0,1,1,0,1,1,1]
        multiDatasetEvaluation.FillDatasetObject(dataset)
        self.assertEqual(dataset.getPrecision(dataset), 4/6)
        return

    def test_getRecall(self):
        dataset = EvaluatedDataset
        dataset.predicted = [1,0,0,1,1,1,0,1,0,0,0,1]
        dataset.actual =    [0,0,1,1,1,0,1,1,0,1,1,1]
        multiDatasetEvaluation.FillDatasetObject(dataset)
        self.assertEqual(dataset.getRecall(dataset), 0.5)
        return

    def test_getNPrecision(self):
        dataset = EvaluatedDataset
        dataset.predicted = [1,0,0,1,1,1,0,1,0,0,0,1]
        dataset.actual =    [0,0,1,1,1,0,1,1,0,1,1,1]
        multiDatasetEvaluation.FillDatasetObject(dataset)
        self.assertEqual(dataset.getNPrecision(dataset), 2/6)
        return

    def test_getNRecall(self):
        dataset = EvaluatedDataset
        dataset.predicted = [1,0,0,1,1,1,0,1,0,0,0,1]
        dataset.actual =    [0,0,1,1,1,0,1,1,0,1,1,1]
        multiDatasetEvaluation.FillDatasetObject(dataset)
        self.assertEqual(dataset.getNRecall(dataset), 0.5)
        return

    def test_getF1Score(self):
        dataset = EvaluatedDataset
        dataset.predicted = [1,0,0,1,1,1,0,1,0,0,0,1]
        dataset.actual =    [0,0,1,1,1,0,1,1,0,1,1,1]
        multiDatasetEvaluation.FillDatasetObject(dataset)
        self.assertAlmostEqual(dataset.getF1Score(dataset), 0.5714,4)
        return

    def test_getScoreWithWeighting(self):
        dataset = EvaluatedDataset
        dataset.predicted = [1,0,0,1,1,1,0,1,0,0,0,1]
        dataset.actual =    [0,0,1,1,1,0,1,1,0,1,1,1]
        multiDatasetEvaluation.FillDatasetObject(dataset)
        self.assertAlmostEqual(dataset.getScoreWithWeighting(dataset, 0.5), 13.5998, 4)
        return

unittest.main()
