#This script holds a function that should be passed a .csv file
#which has two columns, the first being the predicted values, and
#the second being the actual values for a given dataset.

#It should output an evaluation of that model, stored in accessible variables.

#Expanding this to work with multiple file inputs, and those files being passed as arrays to a function.
#Should also be able to do ROC and LOC curves.

import csv
import pandas as pd
pred = []
actu = []

TP = []
FP = []
TN = []
FN = []



#Assume, that if read as a file, that every 2 columns are the results for each dataset.
def OpenFile(fileName):
    global pred
    global actu

    file = open(fileName)
    r = csv.reader(file)
    ncol = len(next(r))
    file.seek(0) #Return to beginning of file

    for i in range(0, ncol):
        pred.append([])
        actu.append([])

    for row in r:
        for i in range(0,ncol-1):
            pred[i].append(int(row[i]))
            actu[i].append(int(row[i+1]))

#Initialises the TP,FP,TN,FP for this model, which are used by the other functions as global variables
def GetConfusionMatrix():
    global TP
    global FP
    global TN
    global FN

    for i in range(0,len(pred)):
        #Should be positive
        if (actu[i] == 1):
            if(pred[i]==1):
                TP+=1
            else:
                FN+=1
        else:
            if(pred[i]==0):
                TN+=1
            else:
                FP+=1

def main():
    OpenFile("dummy.csv")
    GetConfusionMatrix()

    accuracy = (TP+TN)/len(pred)
    precision = TP/(TP+FP)
    recall = TP/(TP+FN)
    f1Score = (2*precision*recall)/(precision+recall)

    print("n = " + str(len(pred)))
    print("Confusion Matrix: ")
    print("   TN=" + str(TN) + " | FP=" + str(FP))
    print("   FN=" + str(FN) + " | TP=" + str(TP))
    print("Predicted N = " + str(TN+FN))
    print("Predicted Y = " + str(FP+TP))
    print("Actual N = " + str(TN+FP))
    print("Actual Y = " + str(FN+TP))
    print("Accuracy = " + str(accuracy))
    print("Precision = " + str(precision))
    print("Recall = " + str(recall))
    print("F1 Score = " + str(f1Score))

main()
